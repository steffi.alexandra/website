from django.conf.urls import url
from .views import tamu, profile, artis, post_new

urlpatterns = [
    url ('tutorial_1.html', profile),
    url ('tutorial_13.html', tamu),
    url ('tutorial_12.html', artis),
    url ('tutorial_14.html', post_new),
    url ('', tamu)
    #re_path(r'^$', tamu, name="index"),
    #<!--re_path(r'tutorial_1.html', profile, name="profile"),
    #re_path(r'tutorial_13.html', tamu, name="tamu"),
    #re_path(r'tutorial_12.html', artis, name="artis"),
    #re_path(r'tutorial_14.html', post_new, name="post_new"),
]