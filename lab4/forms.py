from django import forms

from .models import JadwalPribadi


class TimeInput(forms.TimeInput):
    input_type = 'time'


class PostForm(forms.Form):
    attrs = {
        'class' : 'form-control'
    }

    attrsDate = {
        'class' : 'form-control',
        'type' : 'date'
    }

    attrsTime = {
        'class' : 'form-control',
        'type' : 'time'
    }


    name = forms.CharField(label = 'Event Name', max_length=200, widget=forms.TextInput(attrs=attrs), required = True )
    date = forms.DateField(label = 'Event Date', widget=forms.DateInput(attrs=attrsDate))
    time = forms.TimeField(label = 'Event Time', widget=forms.TimeInput(attrs=attrsTime))
    place = forms.CharField(label = 'Event Place', max_length=200, widget=forms.TextInput(attrs=attrs),required = True)
    category = forms.CharField(label = 'Event Category', max_length=200, widget=forms.TextInput(attrs=attrs), required =True)