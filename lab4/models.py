from django.db import models
from django.utils import timezone


class JadwalPribadi(models.Model):
    name = models.CharField(max_length=200)
    date= models.DateField()
    time= models.TimeField()
    place = models.CharField(max_length=200)
    category = models.CharField(max_length=200)