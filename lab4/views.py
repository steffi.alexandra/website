from django.shortcuts import render
from django.shortcuts import redirect
from .forms import PostForm
from django.http import HttpResponseRedirect
from .models import JadwalPribadi

# Create your views here.
response = {'author' : 'Steffi Alexandra'}
def tamu(request):
    return render(request, 'tutorial_13.html')

def profile(request):
    return render(request, 'tutorial_1.html')

def artis(request):
    return render(request, 'tutorial_12.html')

def post_new(request):
    if 'delete' in request.POST:
        JadwalPribadi.objects.all().delete()
        form = PostForm()
        html = 'tutorial_14.html'
        context = {
                'form' : form,
                'jadwal' : JadwalPribadi.objects.all()
        }
        return render(request, html, context)
    elif request.method == "POST":
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        schedule = JadwalPribadi(name = response['name'], date=response['date'], time=response['time'],  place=response['place'], category = response['category'])
        schedule.save()
        form = PostForm()
        jadwal = JadwalPribadi.objects.all()
        html = 'tutorial_14.html'
        context = {
            'form' : form,
            'jadwal' : jadwal
        }
        return render(request, html, context)
    else:
        form = PostForm()
        jadwal = JadwalPribadi.objects.all()
        context = {
            'form' : form,
            'jadwal' : jadwal
        }
        return render(request, 'tutorial_14.html', context)
