# Generated by Django 2.1.1 on 2018-10-03 00:11

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab4', '0014_auto_20181002_2341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jadwalpribadi',
            name='date_time',
            field=models.DateTimeField(default=datetime.datetime(2018, 10, 3, 0, 11, 18, 666836, tzinfo=utc)),
        ),
    ]
